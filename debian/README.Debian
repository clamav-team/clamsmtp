clamsmtp for Debian
===================
This file lists the build and configuration options used for the clamsmtp
package on the Debian platform.  Please read the clamsmtpd(8) and
clamsmtpd.conf(5) manpages for a more complete discussion of the daemon and its
capabilities.

This package does not attempt to configure your Mail Transfer Agent (MTA) for
content filtering, nor does it attempt to set up your machine as a transparent
proxy, though clamsmtpd has the capability to run in either capacity.  You are
responsible for configuring your MTA or your router.

clamsmtp system user and group
==============================
Although there exists no standard requiring stand-alone daemons to operate
under their own user and group, there are multiple security benefits in doing
so, mostly focusing around the theory of least privilege.

Historically, this package has taken the easy way out (my fault) in having
clamsmtpd run as the clamav user and group, the same user that the clamav
daemon runs as.  Should the clamsmtpd process be compromised, the cracker would
have the same privileges that clamd has.  Now imagine a systems administrator
had added the clamav user to private user groups so that the daemon can scan
protected directories and files.  Those files are now subject to viewing.

clamsmtpd also has the ability to run scripts when a virus is detected.  This
could be another vector for attack by a cracker or even have a potential for
damage by poorly written scripts themselves.  Having clamsmtpd run under its
own user and group would help mitigate potential problems.

The last benefit is to remove an unnecessary version dependency on dpkg, making
the package easier to back-port to Debian Woody, for example.

Debian-specific Directories
============================
/var/run/clamsmtp

    The clamsmtpd.pid file (PidFile) is stored in this directory.  For a daemon
    to place a PID file in /var/run, it must have root privileges.  clamsmtpd
    is run under user "clamsmtp" and group "clamsmtp".

/var/spool/clamsmtp (clamsmtp home directory)

    Temporary and quarantined emails are specified in the config file as
    TempDirectory.  This departs from the upstream default, which is /tmp.
    Emails are potentially confidential or sensitive information, thus warrant
    special attention.

Debian-specific Port Setup
==========================
This package departs from the default clamsmtpd configuration in the ports it
listens to and forward messages to.  The reason for this is partially
historical and partially a compatibility issue.  At the time that postfix was
first introducing its filtering capabilities, it quoted the use of Amavisd and
Amavisd-new as possible filtering proxies.  In those examples, it showed
postfix using port 10025 as the unfiltered port for returning email from the
proxy.  Amavisd-new installs listening to port 10024.  Rather than forcing the
Debian systems administrator from having to customize /etc/postfix/master.cf
yet again, I choose to flip clamsmtpd's settings.

localhost:10026 (For receiving)

    clamsmtpd listens to this port for new SMTP connections.  It scans the DATA
    portion of the email and then returns the email to localhost:10025.

localhost:10025 (For forwarding)

    clamsmtpd expects this port to be run by your MTA.  If it does not find
    service at this port, it will fail to deliver the email.

NOTE: YOU ARE RESPONSIBLE FOR CONFIGURING YOUR MTA OR ROUTER.

Postfix Tips
============
The /usr/share/doc/clamsmtp/postfix.html file included in this package
is taken from the clamsmtp home page and gives instructions for a
basic setup.  Transpose the information about the default port
settings above when editing the /etc/postfix/master.cf and main.cf
files.

Postfix 2.x.x can be more selective about what emails it chooses to
scan.  With mime_header_checks, one can pin-point troublesome emails
by their Content-Type header.  See
/usr/share/doc/clamsmtp/examples/mime_header_checks.

If you are using Postfix, see /usr/share/doc/postfix/FILTER_README.gz
for more information on setting up postfix for content filtering.

